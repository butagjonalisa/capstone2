const mongoose = require("mongoose");

//Schema
const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "First name is required"],
  },
  lastName: {
    type: String,
    required: [true, "Last name is required"],
  },
  email: {
    type: String,
    required: [true, "Email is required"],
  },
  password: {
    type: String,
    required: [true, "Password is required"],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  mobileNo: {
    type: String,
    required: [true, "Mobile No is required"],
  },
  orderedProducts: [
    {
      products: [
        {
          productId: {
            type: mongoose.Schema.Types.ObjectId,
            required: [true, "Product id is required"],
          },
          productName: {
            type: String,
            required: [true, "Product name is required"],
          },
          quantity: {
            type: Number,
            required: [true, "Quantity is required"],
          },
        },
      ],
      totalAmount: {
    type: Number,
  },
  purchasedOn: {
    type: Date,
    default: Date.now,
  },
    },
  ], 
});


//Model
module.exports = mongoose.model("User",userSchema);