const mongoose = require("mongoose");

//Schema
const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Number is required"]
	},
	isActive: {
		type: Boolean,
		default: true,
	},
	createdOn: {
		type: Date,
		default: Date.now,
	},
	userOrders: [
		{
			userId: {
				type: mongoose.Schema.Types.ObjectId,
				required: [true, "Object id is required"]
			},
			orderId: {
				type: String,
				required: [true, "This is optional"]
			},
		}
		]
});

//Model
module.exports = mongoose.model("Product", productSchema);