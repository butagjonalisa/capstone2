const Product = require("../models/Product");
const User = require("../models/User");

// Add Product
module.exports.addProduct = (req, res) => {
  const newProduct = new Product({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  });

  return newProduct
    .save()
    .then((product, error) => {
      if (error) {
        return res.send(false);
      } else {
        console.error(newProduct);
        return res.send(true);
      }
    })
    .catch((err) => res.send(err));
};


module.exports.retrieveAllProducts = (req,res) => {
  return Product.find({}).then(result=> {
    return res.send(result)
  })
}

module.exports.retrieveAllActiveProducts = (req,res) => {
  return Product.find({isActive:true}).then(result=> {
    return res.send(result)
  })
}

module.exports.getProducts = (req,res) => {
  return Product.findById(req.params.productId).then(result => {
    return res.send(result)
  })
}

//Update Product
module.exports.updateProduct = (req,res) => {

  let updatedProduct = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price
  }

  return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((course,error)=> {
    if (error) {
      return res.send(false);
    }else {
      res.send(true);
    }
  })
}

//Archive Product
module.exports.archiveProduct = (req,res) => {
  return Product.findByIdAndUpdate(req.params.productId, {isActive: false}).then(product=>{
    if(product){
      return res.send(true);
    }else{
      return res.send(false);
    }
  })
  .catch(error => {
    return res.send(false);
  });
}

//Activate Product
module.exports.activateProduct = (req,res) => {
  const productId = req.params.productId

  return Product.findByIdAndUpdate(productId, { isActive: true}).then((product,error) => {
    if (error) {
      return res.send(false);
    } else {
      return res.send(true);
    }
  });
};