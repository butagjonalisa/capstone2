//controller
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqbody) => {
  return User.find({ email: reqbody.email }).then((result) => {

    if (result.length > 0) {
      return true;
    } else {

      return false;
    }
  });
};

//Register
module.exports.registerUser = (reqbody) =>{

	let newUser = new User({
		email: reqbody.email,
		password: bcrypt.hashSync(reqbody.password,10),
		mobileNo: reqbody.mobileNo,
		lastName: reqbody.lastName,
		firstName: reqbody.firstName,
	})

	return newUser.save().then((user,error)=>{

		if(error){
			return false;
		}else{
			return true
		}

	})
	.catch(err=>err)
}

//Authentication
module.exports.loginUser = (req, res) => {
  return User.findOne({ email: req.body.email })
    .then((result) => {
      if (result === null) {
        return false;
      } else {
      
        const isPasswordCorrect = bcrypt.compareSync(
          req.body.password,
          result.password
        );
        // If PW matches
        if (isPasswordCorrect) {
          return res.send({ access: auth.createAccessToken(result) });
        }
        // Else PW does not match
        else {
          return res.send(false);
        }
      }
    })
    .catch((err) => res.send(err));
};

module.exports.getProfile = (req, res) => {

  return User.findById(req.user.id)
  .then(result => {
      result.password = "";
      return res.send(result);
  })
  .catch(err => res.send(err))

};

module.exports.order = async (req, res) => {

  console.log(req.user.id); 
  console.log(req.body.productId);

  if(req.user.isAdmin){
    return res.send("Action Forbidden");
  }

  let isUserUpdated = await User.findById(req.user.id).then(user => {
    
    let newOrder = {
      productId: req.body.productId
    }

    user.order.push(newOrder);

    return user.save().then(user => true).catch(err => err.message);

  });

 // Checks if there are errors in updating the user
if (isUserUpdated !== true) {
  return res.send({ message: isUserUpdated });
}

try {
  let isProductUpdated = await Product.findById(req.body.productId);
  
  if (!isProductUpdated) {
    return res.status(404).send({ message: 'Product not found' });
  }

  let newProduct = {
    userId: req.user.id
  };
  
  isProductUpdated.userOrder.push(enrollee);  // Corrected from product.userOder.push(newProduct);
  
  await isProductUpdated.save();  // Corrected from course.save()

  return res.send({ message: 'Product enrollment successful' });
} catch (error) {
  return res.status(500).send({ message: 'Failed to enroll in the product' });
}

  // Checks if there are error in updating the course
  if(isProductUpdated !== true){
    return res.send({message: isProductUpdated});
  }

  // Checks if both user update and course update are successful
  if(isUserUpdated && isProductUpdated){
    return res.send({message: "Ordered Successfully."})
  }
}