## E-COMMERCE API DOCUMENTATION

***TEST ACCOUNTS:***
- Regular User:
     - email: user@mail.com
     - pwd: user123
- Admin User:
    - email: admin@mail.com
    - pwd: admin123
    

***ROUTES:***
- User registration (POST)
	- http://localhost:4000/users/login
    - request body: 
        - email (string)
        - password (string)
- User authentication (POST)
	- http://localhost:4000/users/
    - request body: 
        - email (string)
        - password (string)
- Create Product (Admin only) (POST)
	- http://localhost:4000/products/create
    - request body: 
        - name (string)
        - description (string)
        - price (number)
- Retrieve all products (Admin only) (GET)
	- http://localhost:4000/products/all
    - request body: none