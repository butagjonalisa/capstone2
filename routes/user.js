const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");

const router = express.Router();
const { verify, verifyAdmin } = auth;

//Routes - POST
//Check email
router.post("/checkEmail", (req, res) => {
  userController
    .checkEmailExists(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

//Register a user
router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// User Authentication
router.post("/login", userController.loginUser);

// Details
router.post("/details", verify, userController.getProfile);

// POST route for resetting the password
// router.post("/reset-password", verify, userController.resetPassword);

// Update user profile route
// router.put("/profile", verify, userController.updateProfile);

module.exports = router;