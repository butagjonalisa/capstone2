// Dependencies and Modules
const express = require("express");
const productController = require("../controllers/product");
const auth = require("../auth"); 
const {verify, verifyAdmin} = auth;

// Routing Component
const router = express.Router();

// Create a Product
router.post("/", verify, verifyAdmin, (req, res) => {
  productController.addProduct(req.body).then((result) => res.send(result));
});

//retrieving all products
router.get("/all", (req, res) => {
  productController.getAllProducts(req, res);
});

//Retrieve All Active products
router.get("/", (req, res) => {
  productController.getAllActiveProducts(req, res);
});

//Retrieve Single Product
router.get("/:productId", (req, res) => {
  productController.getProduct(req, res);
});

//Update Product Information
router.put("/:productId", verify, verifyAdmin, (req, res) => {
  productController.updateProduct(req.body).then((result) => res.send(result));
});

//Archive Product
router.put("/:productId/archive", verify, verifyAdmin, (req, res) => {
  productController.archiveProduct(req.body).then((result) => res.send(result));
});

//Activate Product
router.put("/:productId/activate",verify, verifyAdmin, (req, res) => {
  productController.activateProduct(req.body).then((result) => res.send(result));
});


module.exports = router;